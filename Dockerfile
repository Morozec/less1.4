FROM debian:10 as build

ARG  UAJIT2_1=2.1-20210510
ARG  LRUCACHE=0.11
ARG  RESTY_CORE=0.1.22
ARG  DEVEL_KIT=0.3.0
ARG  LUA_NGINX_MODULE=0.10.20
ARG  NGINX=1.17.5


RUN apt update && apt install -y wget gcc make libpcre3-dev zlib1g-dev

RUN wget https://github.com/openresty/luajit2/archive/refs/tags/v${UAJIT2_1}.tar.gz \
    && tar xzvf v${UAJIT2_1}.tar.gz \
    && cd luajit2-${UAJIT2_1} \
    && make && make install

RUN wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v${LRUCACHE}.tar.gz \
    && tar xzvf v${LRUCACHE}.tar.gz \
    && cd lua-resty-lrucache-${LRUCACHE} \
    && make && make install

RUN wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v${RESTY_CORE}.tar.gz \
    && tar xzvf v${RESTY_CORE}.tar.gz \
    && cd lua-resty-core-${RESTY_CORE} \
    && make && make install

RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v${DEVEL_KIT}.tar.gz \
&& tar xvfz v${DEVEL_KIT}.tar.gz \
&& wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v${LUA_NGINX_MODULE}.tar.gz \
&& tar xvfz v${LUA_NGINX_MODULE}.tar.gz 

ENV LUAJIT_LIB=/usr/local/lib
ENV LUAJIT_INC=/usr/local/include/luajit-2.1

RUN wget https://nginx.org/download/nginx-${NGINX}.tar.gz \
&& tar xvfz nginx-${NGINX}.tar.gz \
&& cd nginx-${NGINX} \
&& ./configure --with-ld-opt="-Wl,-rpath,/usr/local/lib" --with-stream --with-pcre --add-module=../ngx_devel_kit-${DEVEL_KIT} --add-module=../lua-nginx-module-${LUA_NGINX_MODULE}\
&& make && make install

FROM debian:10

COPY --from=build /usr/local/lib/ /usr/local/lib/

WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .

RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
